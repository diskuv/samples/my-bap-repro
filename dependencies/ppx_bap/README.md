# PPX BAP

Why is this vendored?

Because as of 2023-12-09, `ppx_bap.opam` had several restrictive constraints:

```perl
  "ppx_assert" {>= "v0.14" & < "v0.16"}
  "ppx_bench" {>= "v0.14" & < "v0.16"}
  "ppx_bin_prot" {>= "v0.14" & < "v0.16"}
  "ppx_cold" {>= "v0.14" & < "v0.16"}
  "ppx_compare" {>= "v0.14" & < "v0.16"}
  "ppx_enumerate" {>= "v0.14" & < "v0.16"}
  "ppx_fields_conv" {>= "v0.14" & < "v0.16"}
  "ppx_hash" {>= "v0.14" & < "v0.16"}
  "ppx_here" {>= "v0.14" & < "v0.16"}
  "ppx_optcomp" {>= "v0.14" & < "v0.16"}
  "ppx_sexp_conv" {>= "v0.14" & < "v0.16"}
  "ppx_sexp_value" {>= "v0.14" & < "v0.16"}
  "ppx_variants_conv" {>= "v0.14" & < "v0.16"}
```
