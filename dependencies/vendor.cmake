# Run with: cmake -P dependencies/vendor.cmake

include("${CMAKE_CURRENT_LIST_DIR}/bap-knowledge/patch.cmake")
include("${CMAKE_CURRENT_LIST_DIR}/regular/patch.cmake")

set(BAP_ARCHIVEFILE ${CMAKE_CURRENT_LIST_DIR}/_temp/bap.zip)
set(BAP_ARCHIVESPACE ${CMAKE_CURRENT_LIST_DIR}/_temp/bap)

set(BAP_KNOWLEDGE_VENDORDIR ${CMAKE_CURRENT_LIST_DIR}/bap-knowledge)
set(REGULAR_VENDORDIR ${CMAKE_CURRENT_LIST_DIR}/regular)
set(MONADS_VENDORDIR ${CMAKE_CURRENT_LIST_DIR}/monads)
set(PPX_BAP_VENDORDIR ${CMAKE_CURRENT_LIST_DIR}/ppx_bap)

# 1. Download from a tag https://github.com/BinaryAnalysisPlatform/bap/tags
#
#   2023-12-09: No tag yet that has the Dune transition https://github.com/BinaryAnalysisPlatform/bap/pull/1565.
#       So use that commit tree https://github.com/BinaryAnalysisPlatform/bap/tree/cbdf732d46c8e38df79d9942fc49bcb97915c657
file(DOWNLOAD "https://codeload.github.com/BinaryAnalysisPlatform/bap/zip/cbdf732d46c8e38df79d9942fc49bcb97915c657" "${BAP_ARCHIVEFILE}" SHOW_PROGRESS)

# 2. Pull out the LICENSE and the lib/{monads,knowledge,regular,bap_types}/ subfolders
file(REMOVE_RECURSE "${BAP_ARCHIVESPACE}")
file(ARCHIVE_EXTRACT INPUT "${BAP_ARCHIVEFILE}" DESTINATION "${BAP_ARCHIVESPACE}"
    PATTERNS */lib/monads */lib/knowledge */LICENSE */lib/regular */lib/bap_types)

# 3. Find the archive subdir (ex. bap-cbdf732d46c8e38df79d9942fc49bcb97915c657)
file(GLOB BAP_CONTENTSUBDIR LIST_DIRECTORIES true RELATIVE "${BAP_ARCHIVESPACE}" "${BAP_ARCHIVESPACE}/*")
list(LENGTH BAP_CONTENTSUBDIR BAP_CONTENTSUBDIR_LEN)
if(NOT BAP_CONTENTSUBDIR_LEN EQUAL 1)
    message(FATAL_ERROR "Expected only one top-level archive directory in ${BAP_ARCHIVESPACE}, not ${BAP_CONTENTSUBDIR}")
endif()
set(BAP_CONTENTDIR "${BAP_ARCHIVESPACE}/${BAP_CONTENTSUBDIR}")

# 4. Do patches
BapKnowledge_PatchAll(INDIR "${BAP_CONTENTDIR}/lib/knowledge" OUTDIR "${BAP_KNOWLEDGE_VENDORDIR}")
Regular_PatchAll(INDIR "${BAP_CONTENTDIR}/lib/regular" OUTDIR "${REGULAR_VENDORDIR}")

# 5. Copy as our own vendored {monads,bap-knowledge,regular}/ subdirs, except [dune]
# which we have a custom version, and any files which were written by a PatchAll().

foreach(vendordir IN ITEMS "${BAP_KNOWLEDGE_VENDORDIR}" "${MONADS_VENDORDIR}" "${REGULAR_VENDORDIR}")
    file(COPY_FILE "${BAP_CONTENTDIR}/LICENSE" "${vendordir}/LICENSE" ONLY_IF_DIFFERENT)
endforeach()

file(GLOB MONADS_SOURCES LIST_DIRECTORIES false "${BAP_CONTENTDIR}/lib/monads/*.ml" "${BAP_CONTENTDIR}/lib/monads/*.mli")
file(COPY ${MONADS_SOURCES} DESTINATION "${MONADS_VENDORDIR}")

file(GLOB REGULAR_SOURCES LIST_DIRECTORIES false "${BAP_CONTENTDIR}/lib/regular/*.ml" "${BAP_CONTENTDIR}/lib/regular/*.mli")
file(COPY  ${REGULAR_SOURCES} DESTINATION "${REGULAR_VENDORDIR}"
    PATTERN regular.ml* EXCLUDE
    PATTERN regular_bytes.ml EXCLUDE
    PATTERN regular_data.ml EXCLUDE
    PATTERN regular_seq.ml EXCLUDE)

# 6. Copy as our own vendored ppx_bap
file(COPY_FILE "${BAP_CONTENTDIR}/LICENSE" "${PPX_BAP_VENDORDIR}/LICENSE" ONLY_IF_DIFFERENT)
