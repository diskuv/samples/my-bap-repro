(** OS abstraction interfaces for knowledge representation *)

(** The flags to {!openfile}. *)
type os_open_flag =
  | O_RDONLY  (** Open for reading *)
  | O_WRONLY  (** Open for writing *)
  | O_RDWR  (** Open for reading and writing *)
  | O_NONBLOCK  (** Open in non-blocking mode *)
  | O_APPEND  (** Open for append *)
  | O_CREAT  (** Create if nonexistent *)
  | O_TRUNC  (** Truncate to 0 length if existing *)
  | O_EXCL  (** Fail if existing *)
  | O_NOCTTY  (** Don't make this dev a controlling tty *)
  | O_DSYNC
      (** Writes complete as `Synchronised I/O data
          integrity completion' *)
  | O_SYNC
      (** Writes complete as `Synchronised I/O file
          integrity completion' *)
  | O_RSYNC
      (** Reads complete as writes (depending
          on O_SYNC/O_DSYNC) *)
  | O_SHARE_DELETE
      (** Windows only: allow the file to be deleted
          while still open *)
  | O_CLOEXEC
      (** Set the close-on-exec flag on the
          descriptor returned by {!OS.openfile}.
          See {!Unix.set_close_on_exec} for more
          information. *)
  | O_KEEPEXEC
      (** Clear the close-on-exec flag.
          This is currently the default. *)

(** Abstraction over parts of Caml_unix so can work (with proper
    implementations) in Javascript and other non-Unix environments *)
module type OS = sig
  type open_flag = os_open_flag
  (** The flags to {!OS.openfile}. *)

  type file_descr
  (** The abstract type of file descriptors. *)

  type file_perm = int
  (** The type of file access rights, e.g. [0o640] is read and write for user,
        read for group, none for others *)

  val openfile : string -> open_flag list -> file_perm -> file_descr
  (** Open the named file with the given flags. Third argument is the
      permissions to give to the file if it is created (see
      {!Unix.umask}). Return a file descriptor on the named file. *)

  val map_file :
    file_descr ->
    ?pos:(* thwart tools/sync_stdlib_docs *) int64 ->
    ('a, 'b) Stdlib.Bigarray.kind ->
    'c Stdlib.Bigarray.layout ->
    bool ->
    int array ->
    ('a, 'b, 'c) Stdlib.Bigarray.Genarray.t
  (** Memory mapping of a file as a Bigarray.
    [map_file fd kind layout shared dims]
    returns a Bigarray of kind [kind], layout [layout],
    and dimensions as specified in [dims].  The data contained in
    this Bigarray are the contents of the file referred to by
    the file descriptor [fd] (as opened previously with
    {!openfile}, for example).  The optional [pos] parameter
    is the byte offset in the file of the data being mapped;
    it defaults to 0 (map from the beginning of the file).
  
    If [shared] is [true], all modifications performed on the array
    are reflected in the file.  This requires that [fd] be opened
    with write permissions.  If [shared] is [false], modifications
    performed on the array are done in memory only, using
    copy-on-write of the modified pages; the underlying file is not
    affected.
  
    [Genarray.map_file] is much more efficient than reading
    the whole file in a Bigarray, modifying that Bigarray,
    and writing it afterwards.
  
    To adjust automatically the dimensions of the Bigarray to
    the actual size of the file, the major dimension (that is,
    the first dimension for an array with C layout, and the last
    dimension for an array with Fortran layout) can be given as
    [-1].  [Genarray.map_file] then determines the major dimension
    from the size of the file.  The file must contain an integral
    number of sub-arrays as determined by the non-major dimensions,
    otherwise [Failure] is raised.
  
    If all dimensions of the Bigarray are given, the file size is
    matched against the size of the Bigarray.  If the file is larger
    than the Bigarray, only the initial portion of the file is
    mapped to the Bigarray.  If the file is smaller than the big
    array, the file is automatically grown to the size of the Bigarray.
    This requires write permissions on [fd].
  
    Array accesses are bounds-checked, but the bounds are determined by
    the initial call to [map_file]. Therefore, you should make sure no
    other process modifies the mapped file while you're accessing it,
    or a SIGBUS signal may be raised. This happens, for instance, if the
    file is shrunk.
  
    [Invalid_argument] or [Failure] may be raised in cases where argument
    validation fails.
    @since 4.06.0 *)

  val close : file_descr -> unit
  (** Close a file descriptor. *)
end
