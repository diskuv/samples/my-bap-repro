# BAP Knowledge

Why is this vendored?

Because as of 2023-12-09, `bap-knowledge.opam` had a restrictive constraint:

```perl
  "core_kernel" {>= "v0.14" & < "v0.16"}
```
