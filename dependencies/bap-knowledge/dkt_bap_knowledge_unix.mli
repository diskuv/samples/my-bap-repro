val set_os_unix : unit -> unit
(** Configures {!Dkt_bap_knowledge.Knowledge} to delegate to the UNIX operating system. *)
