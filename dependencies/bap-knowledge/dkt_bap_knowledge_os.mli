(** Access to OS-es for knowledge representation  *)

include module type of Dkt_bap_knowledge_os_intf

val get_os : unit -> (module OS)
(** Get the implementation of the operating system. The methods of
    the returned first-class module will raise exceptions if
    {!set_os} has not been called. *)

val set_os : (module OS) -> unit
(** Set the implementation of the operating system. *)
