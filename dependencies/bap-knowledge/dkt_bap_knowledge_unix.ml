let set_os_unix () =
  Dkt_bap_knowledge.set_knowledge_os
    (module struct
      type open_flag = Dkt_bap_knowledge_os.os_open_flag
      type file_descr = Caml_unix.file_descr
      type file_perm = Caml_unix.file_perm

      let unix_openflaglist (l : Dkt_bap_knowledge_os.os_open_flag list) =
        List.map
          (function
            | Dkt_bap_knowledge_os.O_RDONLY -> Caml_unix.O_RDONLY
            | O_WRONLY -> O_WRONLY
            | O_RDWR -> O_RDWR
            | O_NONBLOCK -> O_NONBLOCK
            | O_APPEND -> O_APPEND
            | O_CREAT -> O_CREAT
            | O_TRUNC -> O_TRUNC
            | O_EXCL -> O_EXCL
            | O_NOCTTY -> O_NOCTTY
            | O_DSYNC -> O_DSYNC
            | O_SYNC -> O_SYNC
            | O_RSYNC -> O_RSYNC
            | O_SHARE_DELETE -> O_SHARE_DELETE
            | O_CLOEXEC -> O_CLOEXEC
            | O_KEEPEXEC -> O_KEEPEXEC)
          l

      let openfile filename openflaglist =
        Caml_unix.openfile filename (unix_openflaglist openflaglist)

      let map_file = Caml_unix.map_file
      let close = Caml_unix.close
    end)
