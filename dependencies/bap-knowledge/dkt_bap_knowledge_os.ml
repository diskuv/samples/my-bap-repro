include Dkt_bap_knowledge_os_intf

let os_not_set =
  "The BAP knowledge operating system has not been set. Do not use this \
   feature unless on a UNIX/Windows operating system with \
   Dkt_bap_knowledge_unix.set_os_unix performed"

let ref_openfile = ref (fun _ _ _ -> failwith os_not_set)
let ref_map_file_opt = ref None
let ref_close = ref (fun _ -> failwith os_not_set)

module OsReference : OS = struct
  type open_flag = os_open_flag
  type file_descr = unit
  type file_perm = int

  let openfile = !ref_openfile

  let map_file fd =
    match !ref_map_file_opt with
    | None -> failwith os_not_set
    | Some f -> Obj.magic (f fd)

  let close = !ref_close
end

let set_os (os : (module OS)) =
  let module Os = (val os) in
  ref_openfile := Obj.magic Os.openfile;
  ref_map_file_opt := Some (Obj.magic Os.map_file);
  ref_close := Obj.magic Os.close

let get_os () = (module OsReference : OS)
