# This is used by dependencies/vendor.cmake

function(BapKnowledge_PatchFile)
    set(noValues)
    set(singleValues TYPE INFILE OUTFILE)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    file(READ "${ARG_INFILE}" contents)

    # DEVELOPER: Try to make these replacements 1) idempotent and 2) work with any future version of BAP

    # v0.16 introduced Binable.Of_binable_with[out]_uuid and removed the older Binable.Of_binable.
    string(REGEX REPLACE [[Binable\.Of_binable\(]] "Binable.Of_binable_without_uuid(" contents "${contents}")

    # Use wrapped Monads library
    string(REPLACE
        [[open Monads.Std]]
        [[open Dkt_monads.Monads.Std]]
        contents "${contents}")

    # Provide OS shims for Unix module
    string(REPLACE
        [[module Unix = Caml_unix]]
        [[module Unix = (val (Dkt_bap_knowledge_os.get_os ()))]]
        contents "${contents}")

    # And expose a function to set it
    if(ARG_TYPE STREQUAL ml)
        set(contents "${contents}
let set_knowledge_os = Dkt_bap_knowledge_os.set_os
")
    elseif(ARG_TYPE STREQUAL mli)
        set(contents "${contents}
val set_knowledge_os : (module Dkt_bap_knowledge_os.OS) -> unit
(** Set the implementation of the operating system for the knowledge base. *)
")
    endif()

    # Would like: file(WRITE "${ARG_OUTFILE}" "${contents}")
    # but that does not write LF endings on Windows.
    file(CONFIGURE OUTPUT "${ARG_OUTFILE}" CONTENT "${contents}" @ONLY NEWLINE_STYLE UNIX)
endfunction()

function(BapKnowledge_PatchAll)
    set(noValues)
    set(singleValues INDIR OUTDIR)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    BapKnowledge_PatchFile(TYPE ml INFILE "${ARG_INDIR}/bap_knowledge.ml" OUTFILE "${ARG_OUTDIR}/dkt_bap_knowledge.ml")
    BapKnowledge_PatchFile(TYPE mli INFILE "${ARG_INDIR}/bap_knowledge.mli" OUTFILE "${ARG_OUTDIR}/dkt_bap_knowledge.mli")
endfunction()
