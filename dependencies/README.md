# dependencies

Edit the vendored directories by modifying [vendor.cmake](./vendor.cmake)
and then running:

```sh
cmake -P vendor.cmake
```

Some files you must edit directly:

- bap-common/dune
- bap-knowledge/dune
- bap-knowledge/patch.cmake
- monads/dune
- ppx_bap/dune
