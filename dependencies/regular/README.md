# Regular

Why is this vendored?

Because as of 2023-12-09, `regular.opam` had several restrictive constraints:

```perl
  "core_kernel" {>= "v0.14" & < "v0.16"}
```
