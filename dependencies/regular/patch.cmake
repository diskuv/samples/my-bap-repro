# This is used by dependencies/vendor.cmake

function(Regular_PatchFile)
    set(noValues)
    set(singleValues TYPE INFILE OUTFILE)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    file(READ "${ARG_INFILE}" contents)

    # DEVELOPER: Try to make these replacements 1) idempotent and 2) work with any future version of BAP

    # v0.16 introduced Bin_prot.Utils.Make_binable[1]_with[out]_uuid and removed the older Bin_prot.Utils.Make_binable[1].
    string(REGEX REPLACE [[Bin_prot\.Utils\.Make_binable\(]] "Bin_prot.Utils.Make_binable_without_uuid(" contents "${contents}")
    string(REGEX REPLACE [[Bin_prot\.Utils\.Make_binable1\(]] "Bin_prot.Utils.Make_binable1_without_uuid(" contents "${contents}")

    # v0.16 removed all mutators from Ver=Comparable.Make(), including [set].
    string(REGEX REPLACE [[\(Ver\.Map\.set]] "\(Map_intf.Map.set" contents "${contents}")

    # Would like: file(WRITE "${ARG_OUTFILE}" "${contents}")
    # but that does not write LF endings on Windows.
    file(CONFIGURE OUTPUT "${ARG_OUTFILE}" CONTENT "${contents}" @ONLY NEWLINE_STYLE UNIX)
endfunction()

function(Regular_PatchAll)
    set(noValues)
    set(singleValues INDIR OUTDIR)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    Regular_PatchFile(TYPE ml INFILE "${ARG_INDIR}/regular_bytes.ml" OUTFILE "${ARG_OUTDIR}/regular_bytes.ml")
    Regular_PatchFile(TYPE ml INFILE "${ARG_INDIR}/regular_data.ml" OUTFILE "${ARG_OUTDIR}/regular_data.ml")
    Regular_PatchFile(TYPE ml INFILE "${ARG_INDIR}/regular_seq.ml" OUTFILE "${ARG_OUTDIR}/regular_seq.ml")
    Regular_PatchFile(TYPE ml INFILE "${ARG_INDIR}/regular.ml" OUTFILE "${ARG_OUTDIR}/dkt_regular.ml")
    Regular_PatchFile(TYPE mli INFILE "${ARG_INDIR}/regular.mli" OUTFILE "${ARG_OUTDIR}/dkt_regular.mli")
endfunction()
