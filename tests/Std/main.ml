(* Windows ocamldebug:

   opam exec -- dune build tests/Std/main.bc
   #$env:OCAMLLIB="$env:DiskuvOCamlHome\desktop\bc\lib\ocaml"
   #$env:OCAMLFIND_CONF="$env:DiskuvOCamlHome\usr\lib\findlib-precompiled.conf"
   $env:PATH = "Y:\source\dksdk-type\_opam\lib\stublibs;$env:DiskuvOCamlHome\desktop\bc\lib\ocaml\stublibs;$env:DiskuvOCamlHome\desktop\bc\lib\stublibs;$env:PATH"
   ocamldebug _build/default/tests/Std/main.bc
*)

(* open Tezt.Base *)
open MyBapRepro_Std
open KB.Let

(* Why is label failing? *)

let test_label_failing () =
  let nop = MyTheory.Effect.Sort.data "NOP" in
  let no_data = KB.return (MyTheory.Effect.empty nop) in
  let no_ctrl = KB.return (MyTheory.Effect.empty MyTheory.Effect.Sort.fall) in
  let provide_semantics (_ : MyTheory.Label.t) =
    let* label = MyTheory.Label.for_path ~project:"ocaml-backend" "schema" in
    (* let module CT = MyTheory.Empty in *)
    let* (module CT) = MyTheory.current in
    CT.blk label no_data no_ctrl
  in
  KB.promise MyTheory.Semantics.slot provide_semantics;
  let label = KB.Object.create MyTheory.Application.cls in

  let application = Std.Toplevel.eval MyTheory.Semantics.slot label in
  Format.printf "Semantics0(RawMatchData): %a\n%!" KB.Value.pp application
  [@@warning "-unused-value-declaration"]

let () = test_label_failing ()
