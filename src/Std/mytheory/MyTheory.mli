open Dkt_bap_knowledge

module KB = Knowledge
(** Short alias for the Knowledge Base library *)

(** The My Theory. *)
module MyTheory : sig
  type cls
  (** Class index for all Data Theories *)

  type theory = cls KB.obj
  (** A theory instance.
      To create a new theory instance use the {!instance} function.
      To manifest a theory into an OCaml module, use the {!Knowledge.require}
      function. *)

  type t = theory
  (** [MyTheory.t] is {!theory} *)

  type application
  (** Class index for class of applications. *)

  (** The denotation of effects.
  
      An expression sub-language that models data effects (reading and writing
      data).

      An effect is a Knowledge value that is used to give meaning
      to the language forms that do not evaluate to data types but access
      the data of the system.

      All effects belong to the same Knowledge class and share the
      same set of properties, with each property being a specific
      denotation provided by on or more theories.
  *)
  module Effect : sig
    type +'a sort
    (** a type for the effect sort  *)

    type cls
    (** the class of effects.  *)

    type 'a t = (cls, 'a sort) KB.cls KB.value
    (** the effect type is an instance of the Knowledge.value  *)

    val cls : (cls, unit) KB.cls
    (** the class of all effects. *)

    val empty : 'a sort -> 'a t
    (** [empty s] creates an empty effect value.

        The empty effect denotes an absence of any specific knowledge
        about the effects produced by a term.
    *)

    val sort : 'a t -> 'a sort
    (** [sort eff] returns the sort of the effect [eff].  *)

    (** Effect sorts.

        The sort of an effect holds static information that is common
        to all effects of that sort.

        Currently We recognize one kind of effects - [data] effects that
        affect only the values in the computer RAM or storage.

        The [unit] effect represents an effect that is a mixture of all
        the kinds of effects - so all the [data] effects.
    *)
    module Sort : sig
      type +'a t = 'a sort
      type data = private Data
      type ctrl = private Ctrl

      val data : string -> data t
      (** [data kind] defines a data effect of the [kind].  *)

      val ctrl : string -> ctrl t
      (** [ctrl kind] defines a ctrl effect of the [kind]. *)

      val top : unit t
      (** [top] is a set of all possible effects.

          This sort indicates that the statement can have any effect.
      *)

      val bot : 'a t
      (** [bot] is an empty set of effects.

          This sort indicates that the statement doesn't have any
          observable effects, thus it could be coerced to any other
          sort.
      *)

      val both : 'a t -> 'a t -> 'a t
      (** [both s1 s2] an effect of both [s1] and [s2]  *)

      val ( && ) : 'a t -> 'a t -> 'a t
      (** [s1 && s2] is [both s1 s2].  *)

      val union : 'a t list -> 'a t
      (** [union [s1;...;sN] is [s1 && ... && sN].  *)

      val join : 'a t list -> 'b t list -> unit t
      (** [join xs ys] is [union [union xs; union ys ]].  *)

      val order : 'a t -> 'b t -> KB.Order.partial
      (** [order xs ys] orders effects by the order of inclusion.

          [xs] is before [ys] if [ys] includes all effects of [xs],
          otherwise.
      *)

      val rdata : data t
      (** the data read effect.  *)

      val wdata : data t
      (** is the data write effect.  *)

      val fall : ctrl t
      (** the normal control flow effect  *)
    end
  end

  type data = Effect.Sort.data
  type ctrl = Effect.Sort.ctrl

  (** The semantics of applications.

      The semantics of a application is denoted with effects that this
      application produces, so effectively [Semantics = Effect],
      but we reexport it in a separate module here, to separate the
      concerns.
  *)
  module Semantics : sig
    type cls = Effect.cls
    type t = unit Effect.t

    val cls : (cls, unit Effect.sort) Knowledge.cls
    (** the class of program semantics values. *)

    val slot : (application, t) Knowledge.slot
    (** the slot to store program semantics. *)

    include Knowledge.Value.S with type t := t
  end

  (** Denotation of applications. *)
  module Application : sig
    type t = (application, unit) KB.cls KB.value

    val cls : (application, unit) KB.cls

    include Knowledge.Value.S with type t := t
  end

  type label = application KB.Object.t
  (** label is an object of the application class. *)

  module Label : sig
    type t = label

    val project : (application, string) KB.slot
    val path : (application, string) KB.slot
    val fresh : t knowledge
    val null : t
    val for_path : ?package:string -> project:string -> string -> t knowledge

    include Knowledge.Object.S with type t := t
  end

  type 'a effect = 'a Effect.t

  type 'a eff = 'a effect knowledge
  (** Effect *)

  (** The theory of effects.  *)
  module type Effect = sig
    val perform : 'a Effect.sort -> 'a eff
    (** [perform s] performs a generic effect of sort [s].  *)

    val blk : label -> data eff -> ctrl eff -> unit eff
    (** [blk lbl data ctrl] an optionally labeled sequence of effects.

        If [lbl] is [Label.null] then the block is unlabeled. If it is
        not [Label.null] then the denotations will preserve the label
        and assume that this [blk] is referenced from some other
        blocks.

        @since 2.4.0 the [blk] operator accepts (and welcomes)
        [Label.null] as the label in cases when the block is not
        really expected to be called from anywhere else.

    *)
  end

  (** The Minimal theory. *)
  module type Minimal = sig
    include Effect
  end

  (** The Common Theory signature. *)
  module type Common = sig
    include Minimal
  end

  type common = (module Common)
  (** a type abbreviation for the common theory module type.  *)

  module Empty : Common
  (** The empty theory.  *)

  val declare :
    ?desc:string ->
    ?extends:string list ->
    ?context:string list ->
    ?provides:string list ->
    ?package:string ->
    name:string ->
    (module Common) knowledge ->
    unit
  (** [declare name s] that structure [s] as an instance of the Core
      Theory.

      The qualified with the [package] [name] shall be unique,
      otherwise the declaration fails.

      The [extends] list shall contain all theories that are included
      in the structure [s] (except the [Empty] theory, which is
      the base theory of all core theories, so there is no need to add
      it). Failure to list a theory in the [extends] list will prevent
      optimization during theory instantiation and may lead to less
      efficient theories (when the same denotation is computed twice)
      or even conflicts, when the extension overrides the extended
      theory.

      @param context defines a context in which a theory is
      applicable. The declared theory could be only instantiated if
      the context, provided during the instantiation, contains all
      features specified in the [context] argument.

      @param provides is a set of semantic tags that describe
      capabilities provided by the theory. The fully qualified name of
      the theory is automatically provided.
  *)

  val instance :
    ?context:string list -> ?requires:string list -> unit -> theory knowledge
  (** [instance ()] returns the current or creates a new instance of
      the core theory.

      If no parameters of the [instance] function were specialized
      and there is an instance of the current theory then this
      instance is returned.

      Otherwise, the instance is built from all theories that match
      the context specified by the [context] list and provide features
      specified by the [requires] list. If any theory is subsumed by
      other theory, then it is excluded.

      If no instances satisfy this requirement then the empty theory
      is returned. If only one theory satisfies the requirement, then
      it is returned. If many theories match, then a join of all
      theories is computed, stored in the knowledge base, and the
      resulting theory is returned.

      To manifest a theory into an structure, use the [require]
      function. The [current] function is equivalent to the
      composition [instance >=> require], use this function, if you
      want to use the current theory.

      @param context is a set of features that define the context,
      only those theories that have a context that is a subset of
      provided will be instantiated.

      @param requires is a set of semantic features that are
      required. Defaults to the set of all possible features, i.e., if
      unspecified, then all instances applicable to the context will
      be loaded.

      @since 2.4.0 respects the currently set theory.
  *)

  val require : theory -> (module Common) knowledge
  (** [require theory] manifests the [theory] as an OCaml structure.

      It is only possible to create an instance of theory using the
      {!instance} function, but see also {!current}.

      For example, the following will create an theory that is a join
      all currently declared theories (which are not specific to any
      context),

      {[
        let* (module CT) = MyTheory.(instance>=>require) () in
      ]}

      where [let*] comes from [KB.Let].

      To create an instance for a specific context and requirements,

      {[
        MyTheory.instance ()
          ~context:["arm-gnueabi"; "arm"]
          ~require:["bil"; "stack-frame-analysis"] >>=
        MyTheory.require >>= fun (module Common) ->
      ]}

      Finally, to manifest a theory with a specific name, specify the
      name of the theory in the list of requirements.

      {[
        MyTheory.instance ~requires:["bap.std:bil"] () >>=
        MyTheory.require >>= fun (module Common) ->
      ]}
  *)

  val current : (module Common) knowledge
  (** [current] returns the current common theory structure.

      This function is the same as [instance >=> require].

      If there is no current theory set in the contex (via the
      {!with_current} function) then a new structure is created,
      otherwise returns the current selected theory.

      An example of usage,

      {[
        let add x y =
          let* (module CT) = current in
          CT.add x y
      ]}
  *)

  val with_current : theory -> (unit -> 'a knowledge) -> 'a knowledge
  (** [with_current t f] sets [t] as the current theory in the scope of [f].

      Evaluates compuation [f] in the theory of [t]. After [f] is
      evaluated the current theory is restored. This function could be
      safely called recursively, i.e., in a scope of another call to
      [with_current]. Effectively, [with_current], which uses
      [KB.Context.with_var] underneath the hood, creates a dynamic
      scope for the theory variable.
  *)

  (** Documents all declared theories. *)
  module Documentation : sig
    (** Theory documentation.  *)
    module MyTheory : sig
      type t
      (** the documentation  *)

      val name : t -> Knowledge.Name.t
      (** [name theory] is the fully qualified name of [theory].  *)

      val desc : t -> string
      (** [desc theory] is the description provided for [theory].  *)

      val requires : t -> string list
      (** [requires theory] is the list of theory requirements.  *)

      val provides : t -> string list
      (** [provides theory] is the list of theory capabilities.  *)
    end

    val theories : unit -> MyTheory.t list
    (** [theories ()] the declared theories.  *)
  end
end
