open Dkt_bap_knowledge
open Core

let package = "common"

module Effect = MyTheoryEffect

type cls = Application [@@warning "-37"]
type application = cls

let (cls : (cls, unit) Knowledge.cls) =
  Knowledge.Class.declare ~package "application" () ~public:true
    ~desc:"a class of application instances"

let application = cls

let string_property ~desc cls name =
  Knowledge.Class.property ~package cls name Knowledge.Domain.string
    ~persistent:Knowledge.Persistent.string ~public:true ~desc

module Label = struct
  let project =
    string_property cls "source-project" ~desc:"the name of the project"

  let path =
    string_property cls "source-path" ~desc:"the path to the source code"

  let null = Knowledge.Object.null cls
  let fresh = Knowledge.Object.create cls

  let for_path ?package ~project:p_project p_path =
    let open Knowledge.Let in
    let path_id = String.concat ~sep:"/" [ p_project; p_path ] in
    let* obj = Knowledge.Symbol.intern ?package path_id cls in
    let* () = Knowledge.provide project obj p_project in
    let* () = Knowledge.provide path obj p_path in
    Knowledge.return obj

  include (val Knowledge.Object.derive cls)
end

module Semantics = struct
  type cls = Effect.cls

  let cls = Knowledge.Class.refine Effect.cls Effect.Sort.top

  module Self = (val Knowledge.Value.derive cls)

  let slot =
    Knowledge.Class.property ~package application "semantics" Self.domain
      ~persistent:(Knowledge.Persistent.of_binable (module Self))
      ~public:true ~desc:"the application semantics"

  include Self
end

include (val Knowledge.Value.derive cls)
