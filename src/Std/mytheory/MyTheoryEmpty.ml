open Dkt_bap_knowledge
module Value = Knowledge.Value
module Effect = MyTheoryEffect
module KB = Knowledge

module Common (* : Common  *) = struct
  let neweff eff =
   KB.return @@ Value.empty (KB.Class.refine Effect.cls eff)

  (* let data = neweff Effect.Sort.bot *)

  (* let ctrl = neweff Effect.Sort.bot *)
  let unit = neweff Effect.Sort.bot
  let perform eff = neweff eff
  let blk _ _ _ = unit
end
