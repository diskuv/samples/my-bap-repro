open Dkt_bap_knowledge
module KB = Knowledge

module MyTheory = struct
  module Application = Application
  module Effect = MyTheoryEffect
  module Label = Application.Label
  module Semantics = Application.Semantics

  type application = Application.cls
  type 'a effect = 'a Effect.t
  type 'a eff = 'a effect knowledge
  type data = Effect.Sort.data
  type ctrl = Effect.Sort.ctrl
  type label = application Knowledge.Object.t
  type cls = MyTheoryDefinition.theory_cls
  type theory = MyTheoryDefinition.theory
  type t = theory

  module type Effect = MyTheoryDefinition.Effect
  module type Minimal = MyTheoryDefinition.Minimal
  module type Common = MyTheoryDefinition.Common

  type common = (module Common)

  module Empty : Common = MyTheoryEmpty.Common
  include MyTheoryManager
end
