open Dkt_bap_knowledge

let package = "common"

type 'a effect = 'a MyTheoryEffect.t
type 'a eff = 'a effect knowledge
type data = MyTheoryEffect.Sort.data
type ctrl = MyTheoryEffect.Sort.ctrl
type application = Application.cls
type label = application Knowledge.Object.t
type theory_cls

let theory : (theory_cls, unit) Knowledge.cls =
  Knowledge.Class.declare ~package "theory" () ~public:true
    ~desc:"the class of Common Theory instances"

type theory = theory_cls Knowledge.Object.t

module type Effect = sig
  val perform : 'a MyTheoryEffect.Sort.t -> 'a eff
  val blk : label -> data eff -> ctrl eff -> unit eff
end

module type Minimal = sig
  include Effect
end

module type Common = sig
  include Minimal
end
