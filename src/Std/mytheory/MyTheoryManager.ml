open Core
open Dkt_bap_knowledge
open MyTheoryDefinition
open Knowledge.Syntax

module KB = Knowledge

(* let user_package = "user" *)
let package = "common"

module Value = Knowledge.Value
module Name = Knowledge.Name
(* module Desugar = Bap_core_theory_pass.Desugar *)

type 'a theory = {
  name : Set.M(Name).t;
  desc : string;
  desugared : Base.bool;
  requires : Set.M(String).t;
  provides : Set.M(String).t;
  structure : 'a;
  is_empty : Base.bool;
  id : Set.M(Name).t;
}

type common = (module Common)

let known_theories : (Name.t, common knowledge theory) Hashtbl.t =
  Hashtbl.create (module Name)

let features = Set.of_list (module String)
let names = Set.of_list (module Name)

module Empty = struct
  let name = Name.create ~package "empty"
  let requires = [ "empty" ]
  let provides = [ Name.show name ]

  module Self = MyTheoryEmpty.Common

  let theory =
    {
      is_empty = true;
      name = names [ name ];
      desc = "The empty theory.";
      desugared = false;
      requires = Set.of_list (module String) requires;
      provides = Set.of_list (module String) provides;
      structure = (module Self : Common);
      id = names [ name ];
    }
end

let check_uniqueness name =
  match Hashtbl.find known_theories name with
  | None -> ()
  | Some { desc; _ } ->
      invalid_argf
        "Theory.declare: Name exists. A theory with the name `%a' is already \
         declared by some other component. The other component provided %s."
        Name.str name
        (if String.is_empty desc then "no description to its theory"
         else sprintf "the following description to its theory: %S" desc)
        ()

let declare ?(desc = "") ?(extends = []) ?(context = []) ?(provides = [])
    ?package ~name structure =
  let name = Name.create ?package name in
  let extends = List.map extends ~f:Name.read in
  check_uniqueness name;
  Hashtbl.add_exn known_theories ~key:name
    ~data:
      {
        is_empty = false;
        name = names (name :: Empty.name :: extends);
        desugared = false;
        desc;
        structure;
        requires = features context;
        provides = Set.add (features provides) (Name.show name);
        id = names [ name ];
      }

let ( ++ ) x y =
  let+ x = x and+ y = y in
  Value.merge x y
  [@@inline]

let join2s p q s x y =
  let* x = x and+ y = y in
  p s !!x !!y ++ q s !!x !!y
  [@@inline]

module Join (P : Common) (Q : Common) : Common = (* : Common  *)
struct
  let perform s = P.perform s ++ Q.perform s
  let blk l x y = join2s P.blk Q.blk l x y
end

let join_commons (module P : Common) (module Q : Common) : (module Common) =
  (module Join (P) (Q))

(* let equal x y = Set.equal x.name y.name *)

let set_inclusion_order x y : Knowledge.Order.partial =
  if Set.equal x y then EQ
  else if Set.is_subset x ~of_:y then LT
  else if Set.is_subset y ~of_:x then GT
  else NC

let order t1 t2 = set_inclusion_order t1.name t2.name

let merge t1 t2 =
  match order t1 t2 with
  | LT -> t2
  | GT -> t1
  | NC ->
      {
        name = Set.union t1.name t2.name;
        desc = "";
        is_empty = false;
        requires = Set.union t1.requires t2.requires;
        provides = Set.union t1.provides t2.provides;
        structure = join_commons t1.structure t2.structure;
        desugared = false;
        id = Set.union t1.id t2.id;
      }
  | EQ ->
  match (t1.desugared, t2.desugared) with
  | true, false -> t1
  | false, true -> t2
  | _ -> t1

let join t1 t2 = Ok (merge t1 t2)

let sexp_of_name name =
  Sexp.List (Set.to_list name |> List.map ~f:(fun n -> Sexp.Atom (Name.show n)))

let inspect { name; desc; _ } =
  Sexp.List
    (sexp_of_name name :: (if String.is_empty desc then [] else [ Atom desc ]))

let domain =
  Knowledge.Domain.define "theory" ~inspect ~join ~empty:Empty.theory ~order

let slot =
  Knowledge.Class.property theory "instance" domain ~package
    ~desc:"The theory structure"

let current =
  Knowledge.Context.declare ~package "*current-theory*"
    !!(Knowledge.Object.null theory)

module Theory = (val Knowledge.Object.derive theory)

let theories = KB.Context.declare ~package "*theories*" !!None

let theories () =
  KB.Context.get theories >>= function
  | Some theories -> !!theories
  | None ->
      let init = Map.empty (module Theory) in
      Hashtbl.to_alist known_theories
      |> Knowledge.List.fold ~init ~f:(fun theories (name, def) ->
             Knowledge.Symbol.intern (Name.unqualified name) theory
               ~package:(Name.package name)
             >>| fun name -> Map.add_exn theories ~key:name ~data:def)
      >>= fun r ->
      KB.Context.set theories (Some r) >>| fun () -> r

(* let str_ctxt () ctxt = List.to_string (Set.to_list ctxt) ~f:Fn.id *)

let is_applicable ~provided ~requires =
  Set.for_all requires ~f:(Set.mem provided)

let is_required ~required ~provides =
  match required with
  | None -> true
  | Some required -> Set.exists provides ~f:(Set.mem required)

let without_subsumptions theories =
  List.filter theories ~f:(fun t1 ->
      not
      @@ List.exists theories ~f:(fun t2 ->
             match order t1 t2 with LT -> true | _ -> false))

let refine ?(context = []) ?requires theories =
  let provided = features context
  and required = Option.map ~f:features requires in
  without_subsumptions
  @@ List.filter theories ~f:(fun { requires; provides; _ } ->
         is_applicable ~provided ~requires && is_required ~required ~provides)

let theory_for_id id =
  let sym = sprintf "'%s" (List.to_string (Set.to_list id) ~f:Name.show) in
  Knowledge.Symbol.intern sym theory ~package:"core-internal"

let is_instantiated id = Knowledge.collect slot id >>| fun t -> not t.is_empty

(* On instantiation of recursive modules.

   We employ the same techinique as OCaml does [1].
   When a module is instantiated we create an instance
   of this module with an empty structure, so that if
   a module creates an instance of itself during instantiation,
   it will not enter an infinite recursion but will get the
   empty denotation. Our merge operator, which will be called,
   by the provide operator after the final instance is created,
   will notice that the initial structure is ordered strictly
   before the new one (because we used the Empty.name for it),
   thus it will drop it from the final structure.

   [1]: Leroy, Xavier."A proposal for recursive modules in Objective
   Caml." Available from the author’s website (2003).
*)
let instantiate t =
  theory_for_id t.id >>= fun id ->
  is_instantiated id >>= function
  | true -> Knowledge.return id
  | false ->
      Knowledge.provide slot id { Empty.theory with is_empty = false }
      >>= fun () ->
      t.structure >>= fun structure ->
      Knowledge.provide slot id { t with structure } >>| fun () -> id

let instance ?context ?requires () =
  KB.Context.get current >>= fun theory ->
  match (context, requires) with
  | None, None when not (Knowledge.Object.is_null theory) -> !!theory
  | _ -> (
      theories () >>| Map.data >>| refine ?context ?requires >>= function
      | [] -> theory_for_id Empty.theory.id
      | [ t ] -> instantiate t
      | theories -> (
          List.fold theories ~init:(names []) ~f:(fun names t ->
              Set.union names t.id)
          |> theory_for_id
          >>= fun id ->
          is_instantiated id >>= function
          | true -> Knowledge.return id
          | false ->
              Knowledge.List.map theories
                ~f:(instantiate >=> Knowledge.collect slot)
              >>| List.reduce_balanced_exn ~f:merge
              >>= Knowledge.provide slot id
              >>| fun () -> id))

let require name =
  let open Knowledge.Syntax in
  theories () >>= fun theories ->
  match Map.find theories name with
  | Some t -> t.structure
  | None ->
      Knowledge.collect slot name >>= fun t ->
      if t.desugared then !!(t.structure)
      else
        let module CT = (val t.structure) in
        (* let t =
             { t with desugared = true; structure = (module Desugar (CT) : Core) }
           in *)
        KB.provide slot name t >>| fun () -> t.structure

let with_current t f = Knowledge.Context.with_var current t f
let current = (instance >=> require) ()

module Documentation = struct
  module MyTheory = struct
    type t = Name.t * common knowledge theory

    let ( - ) xs name = Set.remove xs (Name.show name)
    let name = fst
    let desc (_, { desc; _ }) = desc
    let requires (_, { requires = fs; _ }) = Set.to_list fs

    let provides (self, { provides = fs; _ }) =
      Set.to_list (fs - Empty.name - self)
  end

  let theories () = Hashtbl.to_alist known_theories
end
