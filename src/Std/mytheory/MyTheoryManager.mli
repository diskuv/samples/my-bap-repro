open Dkt_bap_knowledge
open MyTheoryDefinition

val declare :
  ?desc:string ->
  ?extends:string list ->
  ?context:string list ->
  ?provides:string list ->
  ?package:string ->
  name:string ->
  (module Common) knowledge ->
  unit

val instance :
  ?context:string list -> ?requires:string list -> unit -> theory knowledge

val require : theory -> (module Common) knowledge
val with_current : theory -> (unit -> 'a knowledge) -> 'a knowledge
val current : (module Common) knowledge

module Documentation : sig
  module MyTheory : sig
    type t

    val name : t -> Knowledge.Name.t
    val desc : t -> string
    val requires : t -> string list
    val provides : t -> string list
  end

  val theories : unit -> MyTheory.t list
end
