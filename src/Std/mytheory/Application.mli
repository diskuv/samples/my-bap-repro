(* Similar to Bap.Std.Program *)

open Dkt_bap_knowledge
module Effect = MyTheoryEffect

type cls
type application = cls
type t = (application, unit) Knowledge.cls Knowledge.value

val cls : (application, unit) Knowledge.cls

module Semantics : sig
  type cls = Effect.cls
  type t = unit Effect.t

  val cls : (cls, unit Effect.sort) Knowledge.cls
  val slot : (application, t) Knowledge.slot

  include Knowledge.Value.S with type t := t
end

include Knowledge.Value.S with type t := t

module Label : sig
  open Knowledge

  type t = application obj

  val project : (application, string) slot
  val path : (application, string) slot
  val fresh : t knowledge
  val null : t
  val for_path : ?package:string -> project:string -> string -> t knowledge

  include Knowledge.Object.S with type t := t
end
