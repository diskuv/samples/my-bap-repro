include module type of MyTheory
open Core
open Dkt_bap_knowledge

module Std : sig
  module Toplevel : sig
    exception Conflict of Knowledge.conflict
    (** this exception is raised when the knowledge computation
        enters the inconsistent state.

        @since 2.2.0
    *)

    (** {3 Toplevel variables}  *)

    type 'p var
    (** the type of variables holding property ['p] *)

    val var : string -> 'p var
    (** [var name] creates a fresh variable.

        Creates and declares a fresh new property of the
        [bap:toplevel] class. The name is mangled to prevent clashing
        with existing properties, and each evaluation of this function
        creates a new property that is distinct from any previously
        created properties.
    *)

    val put : 'p var -> 'p knowledge -> unit
    (** [put var exp] evaluates [exp] and sets [var] to its result.

        @raise Conflict if [exp] ends up in the conflicting state.
    *)

    val get : 'p var -> 'p
    (** [get var] reads the value of the variable.

        @raise Not_found if [var] was not set with [put].
    *)

    (** {3 The slot interface}  *)

    val eval : ('a, 'p) Knowledge.slot -> 'a Knowledge.obj knowledge -> 'p
    (** [eval property obj_exp] gets [property] of [obj_exp].

        Evaluates the computation [obj_exp] that shall return an
        object of class ['a] and returns the value ['p] of the specified
        [property].

        @raise Conflict when the knowledge base enters the conflicting
        state.
    *)

    val try_eval :
      ('a, 'p) Knowledge.slot ->
      'a Knowledge.obj knowledge ->
      ('p, Knowledge.conflict) result
    (** [try_eval property object] is like [eval property object] but
        returns [Error conflict] instead of raising an exception. *)

    val exec : unit knowledge -> unit
    (** [exec stmt] executes the side-effectful knowledge computation.

        Executes the statement and updates the internal knowledge base.

        @raise Conflict when the knowledge base enters the conflicting
        state.
    *)

    val try_exec : unit knowledge -> (unit, Knowledge.conflict) result
    (** [try_exec stmt] is like [exec stmt] but returns
        [Error conflict] instead of raising an exception.
    *)

    (** {3 The state interface}  *)

    val set : Knowledge.state -> unit
    (** [set s] sets the knowledge base state to [s].

        Any existing state is discarded.
    *)

    val current : unit -> Knowledge.state
    (** [current ()] is the current state of the knowledge base.  *)

    val reset : unit -> unit
    (** [reset ()] resets the knowledge state to the empty state.

        It is the same as [set @@ KB.empty]
    *)
  end

end
