# My Reproduction

This is a reproducible portion of code that I've been hacking away at.

DO NOT USE THIS CODE IN ANY MEANINGFUL SETTINGS. It is really so
I can communicate bugs, problems and code that could be upstreamed.

Any "My Reproduction" source code not explicitly licensed
in the "Other Licenses" section is licensed under [OSL-3.0](./LICENSE).
Just ask if you need some parts to be upstreamed so I can relicense
with a liberal license.

### Other Licenses

These other parts have their own licenses:

| Project       | License                                     |
| ------------- | ------------------------------------------- |
| Monads        | [MIT](./dependencies/monads/LICENSE)        |
| BAP Knowledge | [MIT](./dependencies/bap-knowledge/LICENSE) |
| PPX BAP       | [MIT](./dependencies/ppx_bap/LICENSE)       |
